/**
 * @file dgesv.c
 * @author Baptiste Dubillaud / Diego Andrade Canosa
 * @date October 6th 2020
 * @brief File containing example of doxygen usage for quick reference.
 *
 * Here typically goes a more extensive explanation of what the header
 * defines. Doxygens tags are words preceeded by either a backslash @\
 * or by an at symbol @@.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "mkl_lapacke.h"


/**
* Generate a square matrix with random values
* @author Diego Andrade Canosa
* @param size the size of the matrix that will be generated
*/
double *generate_matrix(int size)
{
    int i;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    srand(1);

    for (i = 0; i < size * size; i++)
    {
        matrix[i] = rand()%100;
        //matrix[i] = i;
    }

    return matrix;
}

/**
* Generate a square identity matrix.
* @author Baptiste Dubillaud
* @param size the size of the matrix that will be generated
*/
double *generate_matrix_identity(int size)
{
    int i, j;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    for(i = 0; i < size; ++i) {
        for(j = 0; j < size; ++j) {
            if(i == j) matrix[i * size + j] = 1; 
            else matrix[i * size + j] = 0;
        }
    }

    return matrix;
}

/**
* Return the absolute value of a given value
* @author Baptiste Dubillaud
* @param value the value to return as its absolute value
*/
double absolute(double value)
{
    if (value < 0)
        return value * -1;
    else
        return value;
}

/**
* Swap two lines in a matrix
* @author Baptiste Dubillaud
* @param matrix the matrix in which rows are going to be swapped
* @param index_line_1 the index of the first line to move
* @param index_line_2 the index of the second line to move
* @param size_line the size of each rows of the matrix
*/
void swap_lines(double *matrix, int index_line_1, int index_line_2, int size_line)
{
    for(int i = 0; i < size_line; ++i) {
        double save = matrix[index_line_1 * size_line + i];
        matrix[index_line_1 * size_line + i] = matrix[index_line_2 * size_line + i];
        matrix[index_line_2 * size_line + i] = save;
    }
}


/**
* Simplify one line of the given matrix by performing coefficiented soustraction on all
* the elements of a matrix
* @author Baptiste Dubillaud
* @param matrix the matrix in which a row will be simplified
* @param reduced_line_index the index of the line to simplify
* @param reductor_line_index the index of the line used to simplify
* @param size_line the size of each rows of the matrix
* @param value the value used as a coefficient to simplify the line (pivot)
*/
void simplify_lines(double *matrix, int reduced_line_index, int reductor_line_index,
                    int size_line, double value)
{
    int i;
    for(i = 0; i < size_line; ++i) 
        matrix[reduced_line_index * size_line + i] -= matrix[reductor_line_index * size_line + i] * value;
}

/**
* Get the maximum value of a vector
* @author Baptiste Dubillaud
* @param tab the vector in which one the value must be found
* @param size the size of the vector
*/
int get_max_from_tab(double *tab, int size)
{
    double max = tab[0];
    int saved_index = 0;
    for (int i = 0; i < size; ++i)
    {
        if (tab[i] > max) {
            max = tab[i];
            saved_index = i;
        }
    }
    return saved_index;
}

/**
* Get the maximum value of a vector
* @author Baptiste Dubillaud
* @param tab the vector in which one the value must be found
* @param size the size of the vector
*/
void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("\nmatrix: %s \n", name);

    for (i = 0; i < size; i++)
    {
        for (j = 0; j < size; j++)
        {
            printf("%f ", matrix[i * size + j]);
        }
        printf("\n");
    }
}

/**
* Used to compare two matrices. Return an error if an inequality more important than
* the authorized factor Epison is discovered
* @author Baptiste Dubillaud
* @param a the first matrix to compare
* @param b the second matrix to compare
* @param size the size of both matrix
*/
int check_result(double *a, double *b, int size)
{
    int i;
    double epsilon = 0.5;
    for (i = 0; i < size * size; i++)
    {
        if (absolute(a[i] - b[i]) > epsilon) {
            printf("\n###############################\nINCORRECT\n###############################\n");
            return 0;
        }
    }
    return 1;
}

/**
 * Gauss Jordan implementation. Compte the A reversed matrix
 * @param a the A matrix
 * @param b the A' matrix (is A reversed at the end of the compute)
 * @param size_a the size of the a matrix
 */
 void gauss_jordan(double *a, double *a_reversed, int size_a)
{
    int i, j, k, row = 0;
    // for every column of A
    for(j = 0; j < size_a; ++j) {
        // look for the index of the row of the absolute maximum number of the current column (j)
        double max = absolute(a[row * size_a + j]);
        double value;
        k = row;
        for(i = row + 1; i < size_a; ++i) {
            value = absolute(a[i * size_a + j]);
            if(value > max) {
                k = i;
                max = value;
            }
        }

        // the pivot is used to made computes on the matrix
        if(a[k * size_a + j] != 0) {
            double pivot = a[k * size_a + j];
            for(i = 0; i < size_a; ++i) {
                a[k * size_a + i] = a[k * size_a + i] / pivot;
                a_reversed[k * size_a + i] = a_reversed[k * size_a + i] / pivot;
            }
            
            // swap 2 lines on matrices
            if(k != row) {
                swap_lines(a, k, row, size_a);
                swap_lines(a_reversed, k, row, size_a);
            }

            // simplify matrices by performations coefficiented soustractions
            for(i = 0; i < size_a; ++i) {
                if(i != row) {
                    simplify_lines(a_reversed, i, row, size_a, a[i * size_a + j]);
                    simplify_lines(a, i, row, size_a, a[i * size_a + j]);
                }
            }
            row++;
        }
    }
}

/**
 * Performs a multiplication on 2 matrices
 * @param a the A matrix
 * @param b the B matrix
 * @param res the matrix where results are going to be stored
 * @param size the size of the matrices
 */
void matrix_multiplication(double *a, double *b, double *res, int size)
{
    int i, j, k;
    for (i = 0; i < size * size; ++i) res[i] = 0;
    for (i = 0; i < size; ++i) {
        for (j = 0; j < size; ++j) {
            for (k = 0; k < size; ++k)
                res[i * size + j] += a[i * size + k] * b[k * size + j];
        }
    }
}

void solve_system(double *a, double *a_reversed, double *b, double *x, int size_a, int size_b_col, int size_b_row)
{
    gauss_jordan(a, a_reversed, size_a);
    matrix_multiplication(a_reversed, b, x, size_a);
}

void main(int argc, char *argv[])
{

    int size = atoi(argv[1]);

    double *a, *a_save, *a_reversed, *a_ref;
    double *b, *b_save, *b_ref;
    double *x;
    double * mult_res;

    a = generate_matrix(size);
    a_save = generate_matrix(size);
    a_ref = generate_matrix(size);
    a_reversed = generate_matrix_identity(size);
    b = generate_matrix(size);
    b_ref = generate_matrix(size);
    b_save = generate_matrix(size);
    x = generate_matrix(size);
    mult_res = generate_matrix_identity(size);

    printf("\n\n############-----############\n\n");

    // Using MKL to solve the system
    MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
    MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

    clock_t tStart = clock();
    info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, a_ref, lda, ipiv, b_ref, ldb);
    printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    // Using Gauss-Jordan algorithm to solve the system
    tStart = clock();
    MKL_INT *ipiv2 = (MKL_INT *)malloc(sizeof(MKL_INT)*size);
    solve_system(a, a_reversed, b, x, size, size, size);

    printf("Time taken by my implementation: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    // Checking results
    tStart = clock();
    
    matrix_multiplication(a_save, x, mult_res, size);

    if (check_result(mult_res, b, size) == 1)
        printf("Result is ok!\n");
    else
        printf("Result is wrong!\n");

    printf("Time taken to check: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    free(a);
    free(a_save);
    free(a_reversed);
    free(b);
    free(b_save);
    free(x);
    free(mult_res);
}
